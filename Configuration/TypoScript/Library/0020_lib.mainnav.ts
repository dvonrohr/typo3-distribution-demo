# Hauptnavigation

lib.mainnav = HMENU
lib.mainnav {
	wrap = <ul class="nav navbar-nav">|</ul>

	1 = TMENU
	1 {
		NO = 1
		NO.allWrap = <li>|</li>
		NO.stdWrap.htmlSpecialChars = 1

		CUR = 1
		CUR.allWrap = <li class="active">|</li>
		CUR.stdWrap.htmlSpecialChars = 1
	}
}