config {
	htmlTag_stdWrap {
		setContentToCurrent = 1
		cObject = COA
		cObject {
			temp = TEXT
			temp.addParams.class = no-js
			temp.append = TEXT
			temp.append.char = 10
			temp.current = 1
 
			# Kleiner IE7
			10 < .temp
			10.addParams.class = no-js lt-ie9 lt-ie8 lt-ie7
			10.wrap = <!--[if lt IE 7 ]>|<![endif]-->
 
			# IE7
			20 < .temp
			20.addParams.class = no-js lt-ie9 lt-ie8
			20.wrap = <!--[if IE 7 ]>|<![endif]-->
 
			# IE8
			30 < .temp
			30.addParams.class = no-js ie8 oldie
			30.wrap = <!--[if IE 8 ]>|<![endif]-->
 
			40 < .temp
			40.addParams.class = no-js
			40.wrap = <!--[if gt IE 8]>|<![endif]-->
		}
	}

	absRefPrefix = /
	tx_realurl_enable = 1
	prefixLocalAnchors = 1

	disablePrefixComment = 1

	spamProtectEmailAddresses = 1
	spamProtectEmailAddresses_atSubstr = (at)

	compressCSS = 1
	concatenateCss = 1

	compressJs = 1
	concatenateJs = 1
}